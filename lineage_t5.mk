# Inherit some common Lineage stuff.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/languages_full.mk)

# Vendor blobs
$(call inherit-product-if-exists, vendor/fxtec/t5/t5-vendor.mk)

# Device
$(call inherit-product, device/fxtec/t5/device.mk)

# Boot Animtion
TARGET_BOOTANIMATION_HALF_RES := true


# Device identifiers
BUILD_FINGERPRINT := fxtec/t5/t5:8.0.0/OPXS27.109-34-10/5:user/release-keys
PRODUCT_BRAND := fxtec
PRODUCT_DEVICE := t5
PRODUCT_MANUFACTURER := F(x)tec
PRODUCT_MODEL := Pro1
PRODUCT_NAME := lineage_t5
PRODUCT_RELEASE_NAME := t5

PRODUCT_BUILD_PROP_OVERRIDES += \
        PRODUCT_NAME=t5

## TWRP
WITH_TWRP := true
